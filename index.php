<?php

require_once('animal.php');
require_once("Frog.php");
require_once("Ape.php");

$sheep = new animal("shaun");

echo "Name : " .$sheep->name ."<br>";
echo "legs : " .$sheep->legs ."<br>";
echo "cold_blooded : " .$sheep->cold_blooded ."<br> <br>";


$kodok = new Frog("Buduk");
echo "Name : " .$kodok->name ."<br>";
echo "legs : " .$kodok->legs ."<br>";
echo "cold_blooded : " .$kodok->cold_blooded ."<br>";
echo "Jump : ";
echo $kodok->jump() ."<br> <br>";

$sungokong = new Ape("Kera Sakti");
echo "Name : " .$sungokong->name ."<br>";
echo "legs : " .$sungokong->legs ."<br>";
echo "cold_blooded : " .$sungokong->cold_blooded ."<br>";
echo "Yell : " ;
echo $sungokong -> yell() ."<br>";

?>